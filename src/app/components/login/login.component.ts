import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent  implements OnInit {

  constructor() { }
 
  isSubmitted: boolean = false
  loginFormModel: any = {
    email: '',
    password: ''
  }

  onLogin(loginForm: NgForm) {
    if (loginForm.valid) {
      console.log('Email:', loginForm.value.email);
      console.log('Password:', loginForm.value.password);
    }
  }
  ngOnInit() {}

}
