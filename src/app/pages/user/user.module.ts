import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserPageRoutingModule } from './user-routing.module';

import { UserPage } from './user.page';
import { LoginModule } from '../../components/login/login.module';
import { HeaderModule } from 'src/app/components/header/header.module';
import { SignupModule } from 'src/app/components/signup/signup.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserPageRoutingModule,
    LoginModule,
    SignupModule,
    HeaderModule
  ],
  declarations: [UserPage]
})
export class UserPageModule {}
